import {Row, Col, Container} from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import ProductCard from '../components/ProductCard';
import Filter from '../components/Filter';
import AppContext from '../AppContext';
import {useParams,useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Products({product}) {

	const {user} = useContext(AppContext);
	const { productId } = useParams();
	const [products, setProducts] = useState([])
	const history = useNavigate();


	useEffect(() => {
		fetch('https://serene-sierra-98507.herokuapp.com/products/getAllProducts')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const productsArr = (data.map(product => {

				return (
					<ProductCard key={product._id} productProp={product} breakpoint={3}/>
				)

			}))
			setProducts(productsArr)
		})

	}, [product])

	return(
		<>
			<div className = "view">
				<Filter/>
				<div className = "viewContain">
					{products}
				</div>
			</div>
		</>
	);

};