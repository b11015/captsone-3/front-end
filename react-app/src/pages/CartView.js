import {Row, Col, Container} from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import CartCard from '../components/CartCard';
import Filter from '../components/Filter';
import AppContext from '../AppContext';
import {useParams,useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Cart({cart}) {

	const {user} = useContext(AppContext);
	const [carts, setCarts] = useState([])


	useEffect(() => {
		fetch(`https://serene-sierra-98507.herokuapp.com/users/getOrder`, {
			method: 'GET',
			headers: {
			  Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const cartsArr = (data.map(cart => {

				return (
					<CartCard key={cart._id} cartProp={cart} />
				)

			}))
			setCarts(cartsArr)
		})

	}, [cart])

	return(
		<>
			<div>
				<div >
					{carts}
				</div>
			</div>
		</>
	);

};