import { Button, Form } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import ProductTable from '../components/ProductTable';
import { useEffect, useState, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import AppContext from '../AppContext';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';
import '../App.css';

export default function AdminDashboard(){
	const {user} = useContext(AppContext);
	const [products, setProducts] =useState([])

	// MODAL USESTATE
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [newName, setNewName] = useState('');
	const [newDescription, setNewDescription] = useState('');
	const [newCategory, setNewCategory] = useState('');
	const [newPrice, setNewPrice] = useState(0);
	const [newQuantity, setNewQuantity] = useState(true);

	const [isActive, setIsActive] = useState(false);

	// console.log(user.isAdmin)
		useEffect(() => {
		fetch('https://serene-sierra-98507.herokuapp.com/products/getAllProducts')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductTable key={product._id} productProp={product}/>
				)
			}))
		})
		if(newName !== '' && newDescription !== '' && newPrice !== 0){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [newName, newDescription, newPrice, isActive, show]);


	const addProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://serene-sierra-98507.herokuapp.com/products/add`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: newName,
				description: newDescription,
				price: newPrice,
				category: newCategory
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.name !== ''){
				Swal.fire({
					title: 'Product Added successfully',
					icon: 'success',
					text: `${newName} is added to the collection`
				})
				handleClose();
			} else {
				Swal.fire({
					title: 'Product Addition failed',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
	}


	return(
		(user.id === null || user.isAdmin === false)
		?
		<Navigate to="/adminview"/>
		:
		<>
			<h1 className="styleH1Admin">Admin Dashboard</h1>
			<div className="styleBtnAdmin">
				<Button className="styleBtnAdmin mb-3" onClick={handleShow}>Add New Product</Button>

			</div>
			<Table striped bordered hover responsive size="sm">
			      <thead className="table-dark">
			        <tr>
			          <th>Name</th>
			          <th>Description</th>
			          <th className="alignCenter">Price</th>
			          <th className="alignCenter">Availablity</th>
			          <th className="alignCenter">Actions</th>
			        </tr>
			      </thead>
			      <tbody>
			      	{products}
			      </tbody>
		    </Table>
	        <>
	          <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
	          <Form onSubmit={ (e) => addProduct(e)}>
	            <Modal.Header closeButton>
	              <Modal.Title>Add New Product</Modal.Title>
	            </Modal.Header>
	            <Modal.Body>

	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	                  <Form.Label>Name</Form.Label>
	                  <Form.Control
	                    type="text"
	                    placeholder="New Product Name"
	                    required
	                    onChange={e => setNewName(e.target.value)}
	                    autoFocus
	                  />
	                </Form.Group>
	                <Form.Group
	                  className="mb-3"
	                  controlId="exampleForm.ControlTextarea1"
	                >
	                  <Form.Label>Description</Form.Label>
	                  <Form.Control as="textarea" rows={3}
	                  placeholder="New Product Description"
	                  required
	                  onChange={e => setNewDescription(e.target.value)}
	                  />
	                </Form.Group>
	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Price</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Product Price"
	                    required
	                    onChange={e => setNewPrice(e.target.value)}
	                  />
	                </Form.Group>


	            </Modal.Body>
	            <Modal.Footer>
	              <Button variant="secondary" onClick={handleClose}>
	                Close
	              </Button>
	              {isActive
	              	?
		              <Button variant="primary" type="submit">
		                Save Changes
		              </Button>
		              :
		              <Button variant="primary" disabled>
		                Save Changes
		              </Button>
		            }
	            </Modal.Footer>
	            </Form>
	          </Modal>
	        </>
		</>
	)
}
