import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Container } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'


export default function Signup() {

    const { user } = useContext(AppContext)

    const history = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [username, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(username);
    console.log(password);

    useEffect(() => {
        if(firstName !== '' && lastName !== '' &&  username !== ''  && email !== '' && password !== ''){

            setIsActive(true);

        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, username, email, password]);

    function registerUser(e) {

        e.preventDefault();

        fetch('https://serene-sierra-98507.herokuapp.com/users/checkEmailExists', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'info',
                    text: 'Please provide another email'
                })

            } else {

                fetch('https://serene-sierra-98507.herokuapp.com/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        username: username,
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data.email){

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Thank you for registering'
                        })

                        history("/login")

                    } else {

                        Swal.fire({
                            title: 'Registration failed',
                            icon: 'error',
                            text: 'Something went wrong'
                        })
                    }
                })
            }
        })

        setFirstName('');
        setLastName('');
        setUserName('');
        setEmail('');
        setPassword('');
    }

     return (

            user.id !== null ?
            <Navigate to="/courses"/>
            :
            <Container className="w-50">

                <h4 className= "mt-5" style={{
                    display: "flex",
                    justifyContent: "center"
                     }}>
                Sign Up</h4>

                <Card classname="loginCard" class="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>

                        <Form onSubmit={e => registerUser(e)}>
                            <Form.Group className="mb-3 mt-3" controlId="firstName">

                                <Form.Control
                                    type="text"
                                    placeholder="First name"
                                    required
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}

                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="lastName">

                                <Form.Control
                                    type="text"
                                    placeholder="Last name"
                                    required
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}

                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="userName">

                                <Form.Control
                                    type="text"
                                    placeholder="Username"
                                    required
                                    value={username}
                                    onChange={e => setUserName(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="userEmail">

                                <Form.Control
                                    type="text"
                                    placeholder="Email"
                                    required
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Password">

                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    required
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                            </Form.Group>

                            <div style={{
                                display: "flex",
                                justifyContent: "center"
                                 }}>
                                 { isActive ?
                                     <Button variant="dark" type="submit" id="submitBtn" className="mt-3 mb-5" >
                                         Create Account
                                     </Button>
                                     :
                                     <Button variant="secondary" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                                         Create Account
                                     </Button>
                                 }

                            </div>

                            <div style={{
                                display: "flex",
                                justifyContent: "center"
                                 }}>
                                <text className="pt-2 ">Already have an account?</text>
                                <Button className="text-primary" variant="muted" as={Link} to="/Login">Login Here!</Button>

                            </div>

                        </Form>

                    </Card.Body>


                </Card>

            </Container>

        )

    }
