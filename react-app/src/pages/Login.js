import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card} from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'




export default function Login(props) {

    const {user, setUser} = useContext(AppContext)
    console.log(user)

    // State hooks to store the values of the input fields
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('https://serene-sierra-98507.herokuapp.com/users/login', {
            method: "POST",
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(typeof data.accessToken !== "undefined"){

                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)

                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                    text: 'Welcome!'
                })
            } else {

                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Check your credentials'
                })
            }
        })


        setUsername('');
        setPassword('');

        // alert(`${email} has been verified! Welcome back!`);

    }

    const retrieveUserDetails = (token) => {

        fetch('http://localhost:4000/users/getSingleDetails', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })

        })
    }

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(username !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [username, password]);


    return (

        (user.id !== null) ?
        <Navigate to= "/productview"/>
        :

        <Container className="w-50">

            <h4 className= "mt-5" style={{
                display: "flex",
                justifyContent: "center"
                 }}>
            Login</h4>

            <Card classname="loginCard" class="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <Card.Text className="mb-4" style={{
                        display: "flex",
                        justifyContent: "center"
                         }}>
                        Enter your username and password:
                    </Card.Text>
                    <Form onSubmit={e => authenticate(e)}>
                        <Form.Group className="mb-3">

                            <Form.Control
                                type="text"
                                placeholder="Username"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="Password">
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                required
                            />
                        </Form.Group>
                        <div style={{
                            display: "flex",
                            justifyContent: "center"
                             }}>
                            { isActive ?
                                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                                    Submit
                                </Button>
                                :
                                <Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                                    Submit
                                </Button>
                             }
                        </div>

                        <div style={{
                            display: "flex",
                            justifyContent: "center"
                             }}>
                            <text className="pt-2 ">No account yet?</text>
                            <Button className="text-primary" variant="muted" as={Link} to="/Signup">Sign Up Here!</Button>

                        </div>

                    </Form>

                </Card.Body>


            </Card>

        </Container>

    )

}