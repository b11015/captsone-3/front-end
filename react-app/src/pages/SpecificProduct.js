import React, { useState, useContext, useEffect } from 'react';
import {Container,Card,Button} from 'react-bootstrap';
import AppContext from '../AppContext';
import {useParams,useNavigate, Link} from 'react-router-dom';
import pic from '../components/images/tshirt.png';
import Swal from 'sweetalert2';
import './SpecificProduct.css';

export default function SpecificProduct(){

    const {user} = useContext(AppContext);

    const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');


    const history = useNavigate();

    useEffect(() => {
    	console.log(productId)
		fetch(`https://serene-sierra-98507.herokuapp.com/products/viewProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)

		})
	}, [productId])

	const order = (productId, productName, productDescription, productPrice) => {

		fetch('https://serene-sierra-98507.herokuapp.com/users/order', {

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				productDescription: description,
				productPrice: price

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data) {
				Swal.fire({
					title: 'Successfully added to cart',
					icon: 'success'

				})

				history('/productview')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}




    return(
        <Container>
			<Card className='shadow-lg m-2 p-3 rounded align text-center card' text="primary">
                    <Card.Body>
                        <Card.Img className = 'img5' variant="top" src={pic} alt=""/>
                        <Card.Text><h5>{name}</h5></Card.Text>
                        <Card.Text><p>Description: {description}</p></Card.Text>
                        <Card.Text><p>Price:{price}</p></Card.Text>
                    </Card.Body>
				<Card.Footer>
				{user.id !== null ?
					<div className="d-grip gap-2">
						<Button   className ="button mt-3" onClick={() => order(productId)}>Add to Cart</Button>
					</div>
					:
					<Link className="button d-grip gap-2" to="/login">Login to Purchase</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
    )
}
