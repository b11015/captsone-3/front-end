import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import Login from './pages/Login';
import Home from './pages/Home';
import Admin from './pages/Admin';
import Signup from './pages/Signup';
import Logout from './pages/Logout'
import SpecificProduct from './pages/SpecificProduct'
import ProductView from './pages/ProductView';
import CartView from './pages/CartView';
import { AppProvider } from './AppContext';






function App() {

	const [user, setUser] = useState({
	    id: null,
	    isAdmin: null
	})

	const unsetUser = () => {
	  localStorage.clear()
	}

	useEffect(() => {

	  fetch('https://serene-sierra-98507.herokuapp.com/users/getSingleDetails', {
	    method: 'GET',
	    headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    // captured the data of whoever is logged in
	    console.log(data)

	        // set the user states values with the user details upon successful login
	        if(typeof data._id !== "undefined"){
	            setUser({
	                id: data._id,
	                isAdmin: data.isAdmin
	            })
	        } else {

	          // set back the initial state of user
	            setUser({
	                id: null,
	                isAdmin: null
	            })
	        }
	    })

	}, [])

	return(
		<AppProvider value={{user, setUser, unsetUser}}>
			<Router>
				<NavBar/>
				<Container>
					<Routes>
						<Route exact path ="/" element={<Home/>}/>
						<Route exact path ="/home" element={<Home/>}/>
						<Route exact path ="/login" element={<Login/>} />
						<Route exact path ="/signup" element={<Signup/>}/>
						<Route exact path ="/adminview" element={<Admin/>}/>
						<Route exact path ="/productview" element={<ProductView/>}/>
						<Route exact path ="/viewProduct/:productId" element={<SpecificProduct/>}/>
						<Route exact path ="/cart/:userId" element={<CartView/>}/>
						<Route exact path ="/logout" element={<Logout/>}/>


					</Routes>
				</Container>
			</Router>
		</AppProvider>
	);

}

export default App;

