import React, {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';
import CreateProduct from './ProductCard';
import UpdateProduct from './UpdateProduct';
import ArchiveProduct from './Archive';

export default function AdminView(props){

	const {productsData, fetchData} = props
	const [products, setProducts] = useState([])

	useEffect(() => {
		const productArray = productsData.map(product => {
			return(
				<tr>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.quantity}</td>
					<td className={product.isActive ? "text-success":"text-danger"}>
						{product.isActive ? "Available":"Unavailalbe"}
					</td>
					<td><UpdateProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData} /></td>
				</tr>
			)
		})
		setProducts(productArray)
	}, [productsData, fetchData])

	return (
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<CreateProduct fetchData={fetchData}/>
			</div>
			<Table>
				<thead className="bg-dark text-white">
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Availability</th>
					<th colSpan={2}>Actions</th>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		</>
	)
}
