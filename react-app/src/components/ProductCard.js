import { useState, useEffect, useContext } from 'react'
import { Card, Col, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'
import pic from '../components/images/tshirt.png';

export default function ProductCard(props) {

   const { user } = useContext(AppContext);
   const history = useNavigate();

   const{productId} = useParams();

   const buyProduct = (productId) => {

    fetch('http://localhost:4000/users/order', {

        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            productId: productId
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        if(data) {
            Swal.fire({
                title: 'Successfully added to cart',
                icon: 'success'

            })

            history('/productview')
        }else {
            Swal.fire({
                title:'Something went wrong',
                icon: 'error'

            })
        }
    })
   }


    //object destructuring
    const {breakpoint, productProp} = props
    const {name, description, price, _id} = productProp



    return (
        <Col xs={12} md={breakpoint} className="mt-4" >
        <Card className="card1">
            <Card.Body>
                <Card.Img variant="top" src={pic} alt="" />
                <Card.Title className="text-center card2">{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text className="card3">{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

            </Card.Body>
            <Card.Footer className="bgradient">
                { user.id !== null && user.isAdmin !== true
                    ?
                    <div className="d-grid gap-2">
                        <Button onClick={() => buyProduct(productId)}>Add to Cart</Button>
                        <Button as={Link} to={`/viewProduct/${_id}`} className= "btn-primary me-2" style={{
                            flexDirection: "row"}}>View Product</Button>
                    </div>
                    :

                    <>
                        <p>Not yet registered? <Link to="/register" >Register Here</Link></p>
                        <Link className="btn btn-danger" to="/logout" >Log In</Link>
                    </>
                }
            </Card.Footer>

        </Card>
        </Col>

    )
}