import { useContext } from 'react';
import {Navbar, Container, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { FiShoppingCart } from "react-icons/fi";
import{Card, Dropdown, Badge, Button} from 'react-bootstrap';
import AppContext from '../AppContext'
import pic from './images/LOGO.jpg'



export default function NavBar() {

	const {user} = useContext(AppContext);


	return(
	<>
			<Navbar bg="secondary" expand="lg" className=" navMain pt-4 pb-4">

			    <Container>
			       <Link to="/home">
                     <img src={pic} alt = '' height={50} width={200} />
                   </Link>

									 <Navbar.Toggle bg="info" className="toggle btn-outline-light me-3"/>
										<Navbar.Collapse bg="info" id="basic-navbar-nav">
									<form class="container-fluid">
			            		<div class="input-group mt-3 mx-3" >
			              			<input type="text" class="form-control me-2" aria-describedby="basic-addon1"/>
			              				<button class="btn btn-outline-light me-3" type="submit" >Search</button>
			           			 </div>
			    				</form>

			    	<Nav>

			    		{user.isAdmin
			            ?
			            <Nav.Link as={ Link } to="/adminview" className="text-light">Admin Dashboard</Nav.Link>
			            :
			            <Nav.Link as={ Link } to="/adminview" hidden className="text-light">Admin Dashboard</Nav.Link>
		            	}
		            	{(user.id !== null && user.isAdmin !== true)
		            	?
		            	<Button variant="secondary" as={ Link } to= {`/cart/${user.id}`}>
			    				<FiShoppingCart size={30}/>

			    		</Button>
		            	:
		            	<Button variant="light" as={ Link } to={`/cart/${user.id}`} hidden>
			    				<FiShoppingCart size={30}/>
			    		</Button>

		           		 }

			    	    {(user.id !== null)
			    	    	?
			    	    	<>
			    	    	<Nav.Link as={ Link } to="/logout" className="text-light">Logout</Nav.Link>
			    	    	</>
			    	    	:
			    	    	<>
			    	    	<Nav.Link as={ Link } to="/login" className="text-light mt-3 me-2">Login</Nav.Link>
    			            <Nav.Link as={ Link } to="/signup" className="text-light mt-3 me-2">Signup</Nav.Link>
    			            </>
			    	    }



			    	</Nav>
						</Navbar.Collapse>
			    </Container>
			</Navbar>

			<Navbar bg="light" expand="lg">
	            <Container>

	          <Nav className="navs">
			    		<Nav.Link as={Link} to="/home"  className= "me-3 text-dark">Home</Nav.Link>
			    			<Nav.Link as={Link} to="/productview" className= "me-3 text-dark">Products</Nav.Link>
			    	</Nav>

						<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 1</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>

							<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 2</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>

							<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 3</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>

							<Dropdown>
			    			<Dropdown.Toggle className= "me-2" variant="mute">Item Category 4</Dropdown.Toggle>

			    			<Dropdown.Menu>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>







	             </Container>

	        </Navbar>


	</>
	)
}




