import React from 'react';
import { Carousel } from 'react-bootstrap';

import image1 from './images/tshirt.png';
import image2 from './images/delle.jpg';
import image3 from './images/franz.jpg';

const Featured = () => {
  return (
    <Carousel fade={true} pause={false}>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image1}
          alt="First slide"
        />

      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image2}
          alt="Third slide"
        />

      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          style= {{height: 500}}
          src={image3}
          alt="Third slide"
        />

      </Carousel.Item>
    </Carousel>
  )
}



export default Featured;