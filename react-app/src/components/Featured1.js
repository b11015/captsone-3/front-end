import React from 'react';
import {Card,Row,Image, Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import pic1 from './images/tshirt.png'
export default function ProductCard({productProp}){

    const { _id, name, description, price, quantity, picture } = productProp


    return(
            <Row className="mt-3 mb-3 mx-3">
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:100</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:200</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>
                 </Col>
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:300</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:150</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:150</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>
                </Col>
                <Col xs={12} md={4}>
                  <Card className='card3 shadow-lg mt-5 p-3  mx-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><p>Name:</p></Card.Text>
                        <Card.Text><p>Description:</p></Card.Text>
                        <Card.Text><p>Price:150</p></Card.Text>
                        <Card.Text><p>Available:</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>

                </Col>
                </Row>

    )
}

        ProductCard.propTypes = {
        productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired
    })
}
