import { useEffect, useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import AppContext from '../AppContext';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function CartView(props) {


    const { user } = useContext(AppContext);

    const {userId} = useParams();

    const {cartProp} = props
    const {productName, productDescription, productPrice, _id} = cartProp

    const deleteOrder = (id)=>{

            fetch(`http://localhost:4000/users/deleteOrder/${user.id}/${_id}`, {

                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);


                if(data) {
                    Swal.fire({
                        title: 'Successfully Deleted',
                        icon: 'success'

                    })

                }else {
                    Swal.fire({
                        title:'Something went wrong',
                        icon: 'error'

                    })
                }
            })
        }


    return(
        <>

            <Table striped bordered hover responsive size="sm">
                  <thead className="table-dark">
                    <tr>
                      <th className="alignCenter">Name</th>
                      <th className="alignCenter">Description</th>
                      <th className="alignCenter">Price</th>
                      <th className="alignCenter">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td> {productName}</td>
                        <td> {productDescription}</td>
                        <td> {productPrice}</td>
                        <td>
                            <Button onClick={ async() =>{
                             await deleteOrder(_id)
                             window.location.reload(false)
                            }}>Delete</Button>
                        </td>
                    </tr>
                  </tbody>
            </Table>

        </>


    )

}